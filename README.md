<!-- vscode-markdown-toc -->
* 1. [Versions](#Versions)
* 2. [Requirements](#Requirements)
* 3. [Environment variables](#Environmentvariables)
* 4. [Examples](#Examples)
	* 4.1. [Create backup user on MariaDB](#CreatebackupuseronMariaDB)
	* 4.2. [Backup all databases to local directory without S3 and compression](#BackupalldatabasestolocaldirectorywithoutS3andcompression)
	* 4.3. [Backup of individual databases in local directory with compression and without S3](#BackupofindividualdatabasesinlocaldirectorywithcompressionandwithoutS3)
	* 4.4. [Backup of all databases in S3 with compression](#BackupofalldatabasesinS3withcompression)
* 5. [Note of thanks](#Noteofthanks)
* 6. [Attribution](#Attribution)
* 7. [Project-License](#Project-License)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

# MariaDB Backup

Docker Image for MariaDB Backup with S3 and 7-Zip based on Alpine Linux

##  1. <a name='Versions'></a>Versions

Of this project, there is always only one version `latest` and so that the version stays up to date, it is rebuilt weekly.

`latest` [Dockerfile](https://gitlab.com/epicdocker/mariadb-backup/-/blob/master/Dockerfile)

##  2. <a name='Requirements'></a>Requirements

- Docker installed - https://www.docker.com/get-started


##  3. <a name='Environmentvariables'></a>Environment variables

- `MARIADB_HOST` the MariaDB host ***required***
- `MARIADB_PORT` the MariaDB port (default: `3306`)
- `MARIADB_USER` the MariaDB user ***required***
- `MARIADB_PASSWORD` the MariaDB password ***required***
- `MARIADBDUMP_OPTIONS` mariadbdump options (default: `--hex-blob --quote-names --quick --add-drop-table --add-locks --create-options --allow-keywords --disable-keys --extended-insert --single-transaction --comments --no-tablespaces`)
- `MARIADBDUMP_DATABASES` list of databases you want to backup (default: `--all-databases`)
- `MARIADBDUMP_DATABASES_EXCLUDES` list of databases that are not to be backed up, only possible in combination with `MULTI_FILES=true` or `MARIADBDUMP_DATABASES=--all-databases` (default: *empty*)
- `MARIADB_TLS_ENABLE` enable TLS connection to MariaDB server if set `true` (default: `false`)
- `S3_EANBLE` enable S3 upload (default: `false`)
- `S3_ACCESS_KEY` your S3 access key ***required***
- `S3_SECRET_KEY` your S3 secret key ***required***
- `S3_REGION` the S3 bucket region ***required*** (not required for [MinIO](https://minio.io))
- `S3_BUCKET` your S3 bucket path ***required***
- `S3_PREFIX` path prefix in your bucket (default: `backup`)
- `S3_FILENAME` a consistent filename to overwrite with your backup. If not set will use a timestamp.
- `S3_ENDPOINT` the S3 Endpoint URL, for S3 Compliant APIs such as [MinIO](https://minio.io) (default: *empty*)
- `S3_S3V4` set to `true` to enable Signature Version 4, required for [MinIO](https://minio.io) servers (default: `false`)
- `S3_IAMROLE` IAM role (default: *empty*)
- `MULTI_FILES` Allow to have one file per database if set `true` (default: `false`)
- `COMPRESS_ENABLE` enable compression of dump with 7-Zip (default: `true`)
- `COMPRESS_ARGUMENTS` 7-Zip arguments for compression (default: `a -si -t7z -md=20 -m0=lzma2 -mx=9 -mmt=on -aoa`)
- `COMPRESS_ENCRYPTION_PASSWORD` 7-Zip password for encryption (default: *empty*)
- `REMOVE_FILES_AFTERWARDS` remove local files after backup creation. Recommended only in combination with S3 upload (default: `false`)


##  4. <a name='Examples'></a>Examples


###  4.1. <a name='CreatebackupuseronMariaDB'></a>Create backup user on MariaDB

```sql
CREATE USER backup@'%' IDENTIFIED BY 'mySecretPassword';
GRANT SELECT, SHOW VIEW ON *.* TO backup@'%';
```


###  4.2. <a name='BackupalldatabasestolocaldirectorywithoutS3andcompression'></a>Backup all databases to local directory without S3 and compression
```bash
docker run --rm --name mariadb-backup \
  -e MARIADB_HOST="mariadb.example.com" \
  -e MARIADB_USER="backup" \
  -e MARIADB_PASSWORD="mySecretPassword" \
  -v /backup/data:/data:rw \
    registry.gitlab.com/epicdocker/mariadb-backup:latest
```


###  4.3. <a name='BackupofindividualdatabasesinlocaldirectorywithcompressionandwithoutS3'></a>Backup of individual databases in local directory with compression and without S3
```bash
docker run --rm --name mariadb-backup \
  -e MARIADB_HOST="mariadb.example.com" \
  -e MARIADB_USER="backup" \
  -e MARIADB_PASSWORD="mySecretPassword" \
  -e MARIADBDUMP_DATABASES="db_one db_two _db_three" \
  -e COMPRESS_ENABLE="true" \
  -v /backup/data:/data:rw \
    registry.gitlab.com/epicdocker/mariadb-backup:latest
```


###  4.4. <a name='BackupofalldatabasesinS3withcompression'></a>Backup of all databases in S3 with compression

Tested with [MinIO](https://minio.io) server

```bash
docker run --rm --name mariadb-backup \
  -e MARIADB_HOST="mariadb.example.com" \
  -e MARIADB_USER="backup" \
  -e MARIADB_PASSWORD="mySecretPassword" \
  -e S3_ACCESS_KEY="minio_s3_access" \
  -e S3_SECRET_KEY="minio_s3_secret_1234abcd" \
  -e S3_ENDPOINT="https://minio.example.com" \
  -e S3_BUCKET="backup" \
  -e COMPRESS_ENABLE="true" \
  -e REMOVE_FILES_AFTERWARDS="true" \
    registry.gitlab.com/epicdocker/mariadb-backup:latest
```


##  5. <a name='Noteofthanks'></a>Note of thanks

Many thanks to [Johannes Schickling](https://github.com/schickling), the subproject 'mysql-backup-s3' from [dockerfiles](https://github.com/schickling/dockerfiles) was used as a template.


##  6. <a name='Attribution'></a>Attribution 

Template Source: https://github.com/schickling/dockerfiles/tree/master/mysql-backup-s3

Project-Icon: https://www.flaticon.com/free-icon/backup_2818765 - [Backup icons created by Freepik - Flaticon](https://www.flaticon.com/free-icons/backup)


##  7. <a name='Project-License'></a>Project-License

MIT License see [LICENSE](LICENSE)
