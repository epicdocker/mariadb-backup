#!/bin/bash

set -eo pipefail

echo ""
echo "                                  __                       ___  __   ";
echo "                   .-----..-----.|__|.----..-----..-----..'  _||  |_ ";
echo "                   |  -__||  _  ||  ||  __||__ --||  _  ||   _||   _|";
echo "                   |_____||   __||__||____||_____||_____||__|  |____|";
echo "                          |__|                                       ";
echo ""
echo " _______               __         _____   ______      ______               __                  ";
echo "|   |   |.---.-..----.|__|.---.-.|     \ |   __ \    |   __ \.---.-..----.|  |--..--.--..-----.";
echo "|       ||  _  ||   _||  ||  _  ||  --  ||   __ <    |   __ <|  _  ||  __||    < |  |  ||  _  |";
echo "|__|_|__||___._||__|  |__||___._||_____/ |______/    |______/|___._||____||__|__||_____||   __|";
echo "                                                                                        |__|   ";
echo ""

echo "First the set variables are checked"

#### Validation of MariaDB environments
if [ -z "${MARIADB_HOST}" ]; then
  echo "You need to set the 'MARIADB_HOST' environment variable."
  exit 1
fi

if [ -z "${MARIADB_USER}" ]; then
  echo "You need to set the 'MARIADB_USER' environment variable."
  exit 1
fi

if [ -z "${MARIADB_PASSWORD}" ]; then
  echo "You need to set the 'MARIADB_PASSWORD' environment variable."
  exit 1
fi

#### Validation of S3 environments
S3_UPLOAD_ENABLE=false
if [ ! -z "$(echo ${S3_ENABLE} | grep -i -E "(yes|true|1)")" ]; then
  S3_UPLOAD_ENABLE=true

  if [ -z "${S3_ACCESS_KEY}" ]; then
    echo "You need to set the 'S3_ACCESS_KEY' environment variable."
    exit 1
  fi

  if [ -z "${S3_SECRET_KEY}" ]; then
    echo "You need to set the 'S3_SECRET_KEY' environment variable."
    exit 1
  fi

  if [ -z "${S3_BUCKET}" ]; then
    echo "You need to set the 'S3_BUCKET' environment variable."
    exit 1
  fi

  if [ -z "${S3_REGION}" ]; then
    echo "Warning: You did not set the 'S3_REGION' environment variable (not required for MonIO)."
  fi
fi

echo "Set variables successfully checked"
echo "Script variables and functions are set"

MARIADB_HOST_OPTS="--host=${MARIADB_HOST} --port=${MARIADB_PORT} --user=${MARIADB_USER} --password=${MARIADB_PASSWORD}"
if [ ! -z "$(echo ${MARIADB_TLS_ENABLE} | grep -i -E "(yes|true|1)")" ]; then
  MARIADB_HOST_OPTS="${MARIADB_HOST_OPTS} --ssl"
fi

DUMP_START_TIME=$(date +"%Y-%m-%dT%H%M%SZ")
DUMP_FILE_SUFFIX="sql"
USE_COMPRESS=false
if [ ! -z "$(echo ${COMPRESS_ENABLE} | grep -i -E "(yes|true|1)")" ]; then
  USE_COMPRESS=true
  DUMP_FILE_SUFFIX="${DUMP_FILE_SUFFIX}.7z"
fi

COMPRESS_ARGUMENTS_ENCRYPTION=""
if [ "${USE_COMPRESS}" == "true" ] && [ ! -z "${COMPRESS_ENCRYPTION_PASSWORD}" ]; then
  COMPRESS_ARGUMENTS_ENCRYPTION=" -mhe -p${COMPRESS_ENCRYPTION_PASSWORD} "
fi

#### Function: upload to S3
s3upload () {
  _SRC_FILE=$1
  _DEST_FILE=$2
  _AWS_ARGS=""
  _S3_PATH="/"

  if [ ! -z "${S3_PREFIX}" ]; then
    _S3_PATH="/${S3_PREFIX}/"
  fi

  echo "Prepare uploading for '${_SRC_FILE}' on S3 as '${_DEST_FILE}'"
  if [ ! -z "${S3_ENDPOINT}" ]; then
    _AWS_ARGS="${_AWS_ARGS} --endpoint-url ${S3_ENDPOINT}"
  fi

  echo "Uploading '${_DEST_FILE}' on S3 ..."
  aws s3 cp ${_SRC_FILE} s3://${S3_BUCKET}${_S3_PATH}${_DEST_FILE} ${_AWS_ARGS}
  _AWS_RESULT=$?

  if [ ${_AWS_RESULT} != 0 ]; then
    >&2 echo "Error uploading '${_DEST_FILE}' on S3"
  else
    echo "Upload of '${_DEST_FILE}' successfully completed"
  fi
}

#### Function: join array with separator
joinWithSeparator () {
  _IFS=;
  _STRING="${*/#/$1}";
  echo "${_STRING#"$1$1$1"}";
}

echo "Script variables and functions were set successfully"
echo "Environment is configured"

#### if S3_IAMROLE is disabled
if [ -z "$(echo ${S3_IAMROLE} | grep -i -E "(yes|true|1)")" ]; then
  #### env vars needed for aws tools - only if an IAM role is not used
  export AWS_ACCESS_KEY_ID=${S3_ACCESS_KEY}
  export AWS_SECRET_ACCESS_KEY=${S3_SECRET_KEY}
  export AWS_DEFAULT_REGION=${S3_REGION}
fi

#### if S3_S3V4 is enabled
if [ ! -z "$(echo ${S3_S3V4} | grep -i -E "(yes|true|1)")" ]; then
  aws configure set default.s3.signature_version s3v4
fi

echo "Environment configuration completed"
echo "Starting SQL backup"

#### if MULTI_FILES is enabled
if [ ! -z "$(echo ${MULTI_FILES} | grep -i -E "(yes|true|1)")" ]; then
  if [ "${MARIADBDUMP_DATABASES}" == "--all-databases" ]; then
    DATABASES=`mysql ${MARIADB_HOST_OPTS} -e "SHOW DATABASES;" | grep -Ev "(Database|information_schema|performance_schema|mysql|sys|innodb)"`
  else
    DATABASES=${MARIADBDUMP_DATABASES}
  fi

  if [ ! -z "${MARIADBDUMP_DATABASES_EXCLUDES}" ]; then
    MARIADBDUMP_DATABASES_EXCLUDES_ARR=("${MARIADBDUMP_DATABASES_EXCLUDES}")
    echo ${MARIADBDUMP_DATABASES_EXCLUDES_ARR}
  fi

  for DB in ${DATABASES}; do
    if [[ ! " ${MARIADBDUMP_DATABASES_EXCLUDES_ARR[*]} " =~ " ${DB} " ]]; then
      echo "Creating individual dump of '${DB}' from '${MARIADB_HOST}' ..."

      DUMP_FILE_NAME="${DUMP_START_TIME}.${DB}.${DUMP_FILE_SUFFIX}"
      DUMP_FILE_PATH="/data/${DUMP_FILE_NAME}"

      if [ "${USE_COMPRESS}" == "true" ]; then
        mariadb-dump ${MARIADB_HOST_OPTS} ${MARIADBDUMP_OPTIONS} --databases ${DB} | 7z ${COMPRESS_ARGUMENTS} ${COMPRESS_ARGUMENTS_ENCRYPTION} ${DUMP_FILE_PATH}
        DUMP_RESULT=$?
      else
        mariadb-dump ${MARIADB_HOST_OPTS} ${MARIADBDUMP_OPTIONS} --databases ${DB} > ${DUMP_FILE_PATH}
        DUMP_RESULT=$?
      fi

      if [ ${DUMP_RESULT} == 0 ]; then
        echo "Individual dump creation of '${DB}' completed successfully"
        echo ""
      else
        >&2 echo "Error creating dump of '${DB}'"
      fi

      if [ "${S3_UPLOAD_ENABLE}" == "true" ] && [ ${DUMP_RESULT} == 0 ]; then
        if [ -z "${S3_FILENAME}" ]; then
          S3_FILE_NAME="${DUMP_START_TIME}.${DB}.${DUMP_FILE_SUFFIX}"
        else
          S3_FILE_NAME="${S3_FILENAME}.${DB}.${DUMP_FILE_SUFFIX}"
        fi
        s3upload ${DUMP_FILE_PATH} ${S3_FILE_NAME}
      fi
    else
      echo "Database '${DB}' is skipped because it is entered in excludes"
    fi
  done
#### else MULTI_FILES is disabled
else
  DATABASES=${MARIADBDUMP_DATABASES}
  if [ ! -z "${MARIADBDUMP_DATABASES_EXCLUDES}" ] && [ "${MARIADBDUMP_DATABASES}" == "--all-databases" ]; then
    MARIADBDUMP_DATABASES_EXCLUDES_SQL=${MARIADBDUMP_DATABASES_EXCLUDES// /|}
    DATABASES_ARR=$(mysql ${MARIADB_HOST_OPTS} -e "SHOW DATABASES;" | grep -Ev "(Database|${MARIADBDUMP_DATABASES_EXCLUDES_SQL})")
    DATABASES=$(joinWithSeparator '' ${DATABASES_ARR})
  fi

  echo "Creating dump of '${DATABASES}' from '${MARIADB_HOST}' ..."

  DUMP_FILE_NAME="${DUMP_START_TIME}.all.${DUMP_FILE_SUFFIX}"
  DUMP_FILE_PATH="/data/${DUMP_FILE_NAME}"

  if [ "${USE_COMPRESS}" == "true" ]; then
    mariadb-dump ${MARIADB_HOST_OPTS} ${MARIADBDUMP_OPTIONS} --databases ${DATABASES} | 7z ${COMPRESS_ARGUMENTS} ${COMPRESS_ARGUMENTS_ENCRYPTION} ${DUMP_FILE_PATH}
    DUMP_RESULT=$?
  else
    mariadb-dump ${MARIADB_HOST_OPTS} ${MARIADBDUMP_OPTIONS} --databases ${DATABASES} > ${DUMP_FILE_PATH}
    DUMP_RESULT=$?
  fi

  if [ ${DUMP_RESULT} == 0 ]; then
    echo "Dump creation completed successfully"
  else
    >&2 echo "Error creating dump of '${MARIADBDUMP_DATABASES}' databases"
  fi

  if [ "${S3_UPLOAD_ENABLE}" == "true" ] && [ ${DUMP_RESULT} == 0 ]; then
    if [ -z "${S3_FILENAME}" ]; then
      S3_FILE_NAME=${DUMP_FILE_NAME}
    else
      S3_FILE_NAME="${S3_FILENAME}.${DUMP_FILE_SUFFIX}"
    fi
    s3upload ${DUMP_FILE_PATH} ${S3_FILE_NAME}
  fi
fi

#### if REMOVE_FILES_AFTERWARDS is enabled
if [ ! -z "$(echo ${REMOVE_FILES_AFTERWARDS} | grep -i -E "(yes|true|1)")" ]; then
  echo "Removing all local files"
  rm -rf /data/*
fi

echo ""
echo " ______               __                                                     __         __          ";
echo "|   __ \.---.-..----.|  |--..--.--..-----.    .----..-----..--------..-----.|  |.-----.|  |_ .-----.";
echo "|   __ <|  _  ||  __||    < |  |  ||  _  |    |  __||  _  ||        ||  _  ||  ||  -__||   _||  -__|";
echo "|______/|___._||____||__|__||_____||   __|    |____||_____||__|__|__||   __||__||_____||____||_____|";
echo "                                   |__|                              |__|                           ";
echo ""
