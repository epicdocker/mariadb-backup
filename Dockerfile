FROM alpine:latest

LABEL image.name="epicsoft_mariadb_backup" \
      image.description="Docker Image for MariaDB Backup with S3 and 7Zip based on Alpine Linux" \
      maintainer="epicsoft LLC" \
      maintainer.name="Alexander Schwarz <as@epicsoft.one>" \
      maintainer.copyright="Copyright 2022-2024 epicsoft LLC / Alexander Schwarz" \
      license="MIT"

ENV MARIADB_HOST ""
ENV MARIADB_PORT "3306"
ENV MARIADB_USER ""
ENV MARIADB_PASSWORD ""
ENV MARIADBDUMP_OPTIONS "--hex-blob --quote-names --quick --add-drop-table --add-locks --create-options --allow-keywords --disable-keys --extended-insert --single-transaction --comments --no-tablespaces"
ENV MARIADBDUMP_DATABASES "--all-databases"
ENV MARIADBDUMP_DATABASES_EXCLUDES ""
ENV MARIADB_TLS_ENABLE "false"
ENV S3_ENABLE "false"
ENV S3_ACCESS_KEY ""
ENV S3_SECRET_KEY ""
ENV S3_REGION ""
ENV S3_BUCKET ""
ENV S3_ENDPOINT ""
ENV S3_PREFIX "backup"
ENV S3_FILENAME ""
ENV S3_S3V4 "false"
ENV S3_IAMROLE ""
ENV MULTI_FILES "false"
ENV COMPRESS_ENABLE "true"
ENV COMPRESS_ARGUMENTS "a -si -t7z -md=20 -m0=lzma2 -mx=9 -mmt=on -aoa"
ENV COMPRESS_ENCRYPTION_PASSWORD ""
ENV REMOVE_FILES_AFTERWARDS "false"

RUN apk --no-cache add bash \
                       mariadb-client \
                       aws-cli \
                       p7zip

ADD backup.sh backup.sh

CMD ["bash", "backup.sh"]
